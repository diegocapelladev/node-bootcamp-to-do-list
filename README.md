# Projeto de To Do List

## Tecnologias utilizadas

- Node.Js
- Express
- Body-parser
- EJS
- Nodemon

## Exec
- Instale as dependências com: `yarn` ou `npm install`
- Inicie o projeto com: `yarn dev` ou `npm run dev`
- Acesse em: http://localhost:5000

## Projeto final

<img src="./capa.png" />